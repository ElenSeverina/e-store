export enum Mode {
  production = 'production',
  development = 'development'
}

export interface ArgvMode {
  mode?: Mode;
}
